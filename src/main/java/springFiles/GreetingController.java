package springFiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springFiles.convert.Convertor;
import springFiles.domain.User;
import springFiles.model.UserRequest;
import springFiles.model.UserResponse;
import springFiles.repos.UserRepos;


@RestController
@RequestMapping("/user")
public class GreetingController {

    @Autowired
    private UserRepos userRepos;

    @PostMapping
    public ResponseEntity<User> add(@RequestBody UserRequest info){
        Convertor convertor = new Convertor();
        User us = convertor.toEntity(info);
        userRepos.save(us);
        return ResponseEntity.ok().body(us);
    }

    @GetMapping
    public ResponseEntity<UserResponse> getUserId(@RequestParam String id){
        Convertor convertor = new Convertor();
        User user = userRepos.findById(id).get();
        UserResponse person = convertor.toResponce(user);
        return ResponseEntity.ok().body(person);
    }

}