package springFiles.model;

import springFiles.domain.User;

import java.time.LocalDate;

public class UserResponse {

    User user;
    LocalDate requestTime;

    public User getUser() {
        return user;
    }

    public UserResponse setUser(User user) {
        this.user = user;
        LocalDate time = LocalDate.now();
        setRequestTime(time);
        return this;
    }


    private LocalDate getRequestTime() {
        return requestTime;
    }

    private void setRequestTime(LocalDate requestTime) {
        this.requestTime = requestTime;
    }
}
