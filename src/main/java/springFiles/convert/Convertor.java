package springFiles.convert;

import org.springframework.stereotype.Component;
import springFiles.domain.User;
import springFiles.model.UserRequest;
import springFiles.model.UserResponse;

@Component
public class Convertor {

    public User toEntity(UserRequest user){
        User us = new User(user.getName(), user.getLastName());
        return us;
    }

    public UserResponse toResponce(User user) {
        UserResponse person = new UserResponse().setUser(user);
        return person;
    }

}
