package springFiles.domain;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="users")
public class User {
    /**
     * Идентификатор
     */
    User(){
    }

    /**
     * Конструктор
     * @param name - имя
     * @param lastName - фамилия
     */
    public User(String name, String lastName){
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.lastName = lastName;
    }
    @Id
    private String id;
    /**
     * Имя человека
     */
    private String name;
    /**
     * Фамилия человека
     */
    private String lastName;

    /**
     * @param id идентификатор
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param name Имя пользователя
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param lastName Фамилия
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return id пользователя
     */
    public String getId() {
        return id;
    }

    /**
     * @return имя пользователя
     */
    public String getName() {
        return name;
    }

    /**
     * @return фамилия пользователя
     */
    public String getLastName() {
        return lastName;
    }

}
