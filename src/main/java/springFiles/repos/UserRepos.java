package springFiles.repos;

import org.springframework.data.repository.CrudRepository;
import springFiles.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepos extends CrudRepository<User, String> {
    Optional<User> findById(String id);
}
